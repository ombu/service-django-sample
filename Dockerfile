FROM python:3.7-slim

MAINTAINER OMBU

WORKDIR /var/www

COPY requirements/base.txt requirements/base.txt

# See https://github.com/dalibo/temboard/commit/ff98d6740ae11345658508b02052294d6cffd448
# for the mkdir /usr/share/man/shennanigans
RUN set -ex && mkdir -p /usr/share/man/man1 /usr/share/man/man7 && \
    apt-get update && \
    apt-get install -y --no-install-recommends postgresql-client && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean && \
    pip install -r requirements/base.txt

COPY . .

ENV DJANGO_SETTINGS_MODULE=website.settings.docker

EXPOSE 8000

ENTRYPOINT  ["./wait-for-postgres.sh"]

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
