from website.settings.base import *

DATABASES['default']['NAME'] = 'django'
DATABASES['default']['USER'] = 'user'
DATABASES['default']['PASSWORD'] = 'meh'
DATABASES['default']['HOST'] = 'db'
DATABASES['default']['PORT'] = '5432'

ALLOWED_HOSTS = ['localhost']

INTERNAL_IPS = ['localhost', '127.0.0.1']

BASE_URL = "localhost:8001"

DEBUG = True