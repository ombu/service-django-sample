import os
from website.settings.base import *
import http.client

DATABASES['default']['NAME'] = 'django'
DATABASES['default']['USER'] = 'root'
DATABASES['default']['PASSWORD'] = 'changeme'
DATABASES['default']['HOST'] = os.environ['DATABASE_ENDPOINT']
DATABASES['default']['PORT'] = '5432'

STATIC_ROOT = '/var/www/static/'
STATIC_URL = '/static/'
MEDIA_ROOT = '/var/www/media/'
MEDIA_URL = '/media/'

def get_local_ip():
    try:
        connection = http.client.HTTPConnection("169.254.169.254", timeout=2)
        connection.request('GET', '/latest/meta-data/local-ipv4')
        response = connection.getresponse()
        return response.read().decode('utf-8')
    except Exception:
        return None

ALLOWED_HOSTS = ['.elb.amazonaws.com', get_local_ip()]

DEBUG = True

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'DEBUG',
        'handlers': ['console'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'level': 'WARNING',
            'handlers': ['console'],
            'propagate': True,
        },
        'django.template': {
            'level': 'WARNING',
            'handlers': ['console'],
            'propagate': False,
        },
        'django.db': {
            'level': 'WARNING',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}