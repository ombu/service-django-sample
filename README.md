Sample Django service
=====================

Run locally
-----------

    docker-compose build
    docker-compose up -d
    docker-compose exec web python manage.py migrate

Visit <http://localhost:8001/>

Build and push remote image
---------------------------

Push it to the remote repository:

    export AWS_ACCOUNT_ID=`aws sts get-caller-identity --output text --query 'Account'`

    docker-compose -p service-django-sample-remote \
                   -f docker-compose-remote.yml build
    
    `aws ecr get-login --no-include-email --region us-west-2`
   
    docker tag service-django-sample-remote_web \
        ${AWS_ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com/ombu/sample-service-django

    docker push ${AWS_ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com/ombu/sample-service-django
